#!/bin/bash
# 
# Maneage buildbot removal script.
# The script would stop the relevant buildbot master and, if
# applicablbe, worker services after which it would remove their
# corresponding directories.
#
# Copyright (C) 2020 Mohammadreza Khellat <mkhellat@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


#--------------------------------------------------------------------
# Script definitions
#
set -e


echo
echo 'Stopping maneage buildbot worker service...'
echo
cd /home/cuser/buildbot-maneage/worker
source sandbox/bin/activate
buildbot-worker stop maneage_worker_1

echo
echo 'Stopping maneage buildbot master service...'
echo
cd /home/cuser/buildbot-maneage/master
source sandbox/bin/activate
buildbot stop maneage_master

echo
echo 'Killing other relevant instances...'
echo
_plist="$(ps aux                                   | grep '/home/cuser/buildbot-maneage'           | grep -E -v 'grep|bash'                  | awk '{print $2}')"
if [[ -n "${_plist}" ]]; then
    kill ${_plist} 2>&1 > /dev/null
fi

echo
echo 'Removing the buildmaster directory...'
echo
cd /home/cuser/buildbot-maneage
rm -rf /home/cuser/buildbot-maneage/master


echo
echo 'Removing the buildworker directory...'
echo
cd /home/cuser/buildbot-maneage
rm -rf /home/cuser/buildbot-maneage/worker



exit 0

# END
