#!/bin/bash
# 
# Maneage 'project configure' execution script.
# The script would be automatically run by the BuildBot builder during
# the 'projcet configure' step.
#
# Copyright (C) 2020 Mohammadreza Khellat <mkhellat@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


#--------------------------------------------------------------------
# Script definitions
#
set -euo pipefail


if [[ -n $(uname -a | grep -i linux) ]]; then
    /home/cuser/buildbot-maneage/worker/maneage_worker_1/maneage-builder/build/project configure                    \
              --build-dir=/home/cuser/buildbot-maneage/_build_output_/build       \
              --input-dir=/home/cuser/buildbot-maneage/_build_output_/input       \
              --software-dir=/home/cuser/buildbot-maneage/_build_output_/software \
              --all-highlevel
elif [[ -n $(uname -a \| grep -i darwin) ]]; then
    /home/cuser/buildbot-maneage/worker/maneage_worker_1/maneage-builder/build/project configure                    \
              --build-dir=/home/cuser/buildbot-maneage/_build_output_/build       \
              --input-dir=/home/cuser/buildbot-maneage/_build_output_/input       \
              --software-dir=/home/cuser/buildbot-maneage/_build_output_/software
else
    echo "Unsupported operating system!"
    exit 1
fi


exit 0

# END
