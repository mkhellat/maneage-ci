# Continuous Integration for Maneage
This is a collection of resources that can be used for the purpose of automating build tests, on specific platforms, when the software packages used for the Maneage core project are changed/updated.

## BuildBot Sample Configuration
You can easily run `./maneage-ci-bb configure` with no flags to
- create and start a BuildBot Master under `$HOME/buildbot-maneage/master`,
- create and start a BuildBot Worker under `$HOME/buildbot-maneage/worker`, and
- create `$HOME/buildbot-maneage/_build_output_` under which Maneage build, software, and input directories would be created.

You can override the default configuration using
- `-p, --parent-dir` option for `BUILDBOT_PARENT_DIR` : where the local master and worker would be created,
- `-b, --build-parent-dir` option for `MANEAGE_BUILD_TOP_DIR`: where Maneage main directories would be created, and
- `-l, --local-worker` and `-r, --remote-worker` options to configure BuildBot workers.

Trigger `./maneage-ci-bb -h` to check the full list of available actions and options.

### Initiate a Test Build
When the sample BuildBot project tree is successfully created, the relevant BuildBot Master and Worker would also be started and you can find the BuildBot web application at the default location `http://localhost:8010`. From there,
- go to _Build > Builders_ to check the list of available builders.
- check if under the _workers_ column you see an available (green) worker.
- if a worker is available, click on _maneage-builder_ and initiate a build by clicking on _force_ at the top of the page.

### Modifying the BuildBot configuration
The full configuration of the created BuildBot project can be found in the file `master.cfg` under the BuildBot Master directory, i.e. `$HOME/buildbot-maneage/master/maneage_master` or `$BUILDBOT_PARENT_DIR/master/maneage_master`. The configuration file is sufficiently commented to understand how you can modify it.

We suggest to follow the logical order mentioned in [BuildBot Documentation - Section 1.4](http://docs.buildbot.net/latest/tutorial/further.html) when modifying the configuration:
#### __Builders__
```python
...
# step 1: create the required directories                                                                                                                                              
createdirs = steps.ShellCommand(                                                                                                                                                           
    name="mkdir build input software",                                                                                                                                                     
    command=["mkdir",                                                                                                                                                                      
             "-p",                                                                                                                                                                         
             "${MANEAGE_BUILD_TOP_DIR}/build",                                                                                                                                             
             "${MANEAGE_BUILD_TOP_DIR}/input",                                                                                                                                             
             "${MANEAGE_BUILD_TOP_DIR}/software"],                                                                                                                                         
    haltOnFailure=True,                                                                                                                                                                    
    description="creating project directories")
    ...
```
#### __Schedulers__
```python
...
# define commit triggered build                                                                                                                                                               
masterchanged = schedulers.SingleBranchScheduler(                                                                                                                                             
    name="all",                                                                                                                                                                               
    change_filter=util.ChangeFilter(branch='master'),                                                                                                                                         
    treeStableTimer=None,                                                                                                                                                                     
    builderNames=["${MANEAGE_BUILDER_NAME}"])
...
```
#### __Sources__
```python
...
gitpoller = changes.GitPoller(                                                                                                                                                                
    '${MANEAGE_GIT_REPO}',                                                                                                                                                                    
    workdir='gitpoller-workdir',                                                                                                                                                              
    branch='master',                                                                                                                                                                          
    pollInterval=300)
...
```
#### __Workers__
```python
c['workers'] = [                                                                                                                                                                              
    worker.Worker('${BUILDWORKER_ID}',                                                                                                                                                        
                  '${BUILDWORKER_PASS}'),                                                                                                                                                     
    worker.Worker('maneage_worker2',                                                                                                                                                          
                  'testpass2')]
...
```
After you are done with the modification, you should `restart` or `sighup` the very BuildBot Master and the Worker(s), for instance in our case with the following commands from the master and worker sandboxed virtualenvs
```shell
$ buildbot restart maneage_master
$ buildbot-worker restart maneage_worker_1
```
If the sanboxed virtualenvs to run the above commands are not activated, you can manually activate them by
```shell
$ cd ${BUILDBOT_PARENT_DIR}/master && source sandbox/bin/activate
$ cd ${BUILDBOT_PARENT_DIR}/worker && source sandbox/bin/activate
```
For the reference, we have also uploaded the sample BuildBot project tree under [sample buildbot tree](sample_buildbot_tree) removing the content of the sandbox directory as well as twistd related files and other logs.

### Adding a new worker
To add a new worker to an existing BuildBot implementation, deploy a BuildBot worker on the system which is supposed to host the worker
```shell
[ USER@WORKER-HOST ]$ mkdir <new_worker_dir>; cd <new_worker_dir>
[ USER@WORKER-HOST ]$ python3 -m venv sandbox
[ USER@WORKER-HOST ]$ source sandbox/bin/activate
(sandbox)[ USER@WORKER-HOST ]$ pip install --upgrade pip
(sandbox)[ USER@WORKER-HOST ]$ pip install buildbot-worker
(sandbox)[ USER@WORKER-HOST ]$ buildbot-worker create-worker <new_worker_name> <master_ip_address> <new_worker_id> <new_worker_pass>
```
We would need network connectivity between the BuildBot worker and the BuildBot master. For instance, if you are hosting the new worker on a VirtualBox installed on your BuildBot master host, a straightforward way to do so is to setup a Host-Only adapter on the virtual machine and properly set IP addresses on both your master host and the virtual machine. It is also a goog idea to add some descriptive information regarding the created worker in the corresponding `info/host` and `info/admin` files on the worker host.

Now, we have to configure the BuildBot master for this new worker by adding a simple `worker.Worker` entry to the workers section of `master.cfg`
```python
c['workers'] = [ ...,      
    worker.Worker('<new_worker_id>',                                                         
                  '<new_worker_pass>')]
```
For the master to read the new configuration, we need to send a hang-up signal to the master by running the following command in the sanboxed virtualenv of the BuildBot master
```shell
(sandbox)[ USER@MASTER-HOST ]$ buildbot sighup <master_name>
```
You need to check `twistd.log` to make sure that the master is running without issues after loading the new configuration.

If everything is fine and the configuration has been successfully reread by the master, we can now start the worker to make it listen to the master on the configured port (by default TCP: `9989`).
```shell
(sandbox)[ USER@WORKER-HOST ]$ buildbot-worker start <new_worker_name>
```
To make sure that the worker has established connection with the master, you can use `netstat` or check the worker `twistd.log`.
```shell
[ USER@WORKER-HOST ]$ netstat -an | grep 9989
tcp4  0  0  <new_worker_ip_address>.<source_port>   <master_ip_address>.9989    ESTABLISHED
```
If everything is fine, we can add the new worker to the list of available workers for the relevant builder in `master.cfg`; for instance, in our sample configuration:
```python
c['builders'] = [
    util.BuilderConfig(
	name="<master_name>",
        workernames=[
	    '<first_worker_id>',
	    '<second_worker_id>',
	    ...,
	    <new_worker_id>
	],
        factory=f_maneage_build)
]
```
Finally, we need to send another SIGHUP to the buildbot master to reread the configuration:
```shell
(sandbox)[ USER@MASTER-HOST ]$ buildbot sighup <master_name>
```

---
## Copyright information
This project is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option)
any later version.

This project is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along
with this project.  If not, see [https://www.gnu.org/licenses](https://www.gnu.org/licenses).
